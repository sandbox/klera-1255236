<?php
/**
 * @file
 * Administrative page callbacks for the sniffer module.
 */

/**
 * sniffer module settings form.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function sniffer_admin_settings() {
	$form['sniffer_row_limit'] = array(
		'#type' => 'select',
		'#title' => t('Discard log entries above the following row limit'),
		'#default_value' => variable_get('sniffer_row_limit', 1000),
		'#options' => drupal_map_assoc(array(100, 1000, 10000, 100000, 1000000)),
		'#description' => t('The maximum number of rows to keep in the database log. Older entries will be automatically discarded. (Requires a correctly configured <a href="@cron">cron maintenance task</a>.)', array('@cron' => url('admin/reports/status')))
	);

	$form["sniffer_use_browscap"]=array(
		"#type"=>'checkbox',
		"#title"=>"Use get_browser() to detect User-Agent ",
		"#default_value"=>variable_get("sniffer_use_browscap", 0),
	);

	$form["sniffer_uids"]=array(
		"#type"=>"textarea",
		"#title"=>"UID`s to track",
		"#default_value"=>variable_get("sniffer_uids", ''),
		'#description' => t('comma-separated, without spaces' )  ,
	);

		$form["sniffer_ips"]=array(
		"#type"=>"textarea",
		"#title"=>"IP`s to track",
		"#default_value"=>variable_get("sniffer_ips", ''),
		'#description' => t('comma-separated, without spaces' )  ,
	);

  return system_settings_form($form);
}

/**
 * Menu callback; displays a listing of log messages.
 */
function sniffer_overview() {
  $filter = sniffer_build_filter_query();
  $rows = array();

  $output = drupal_get_form('sniffer_filter_form');

  $header = array(
		array('data' => t('Date'), 'field' => 'w.wid', 'sort' => 'desc'),
		array('data' => t('User'), 'field' => 'u.name'),
		array('data' => t('IP'), 'field' => 'w.hostname'),
		t('Location'),
		t('POST'),
  );

  $sql = "SELECT w.wid, w.uid,  w.timestamp, w.hostname,  u.name, w.location, w.post FROM {sniffer_data} w INNER JOIN {users} u ON w.uid = u.uid";
  $tablesort = tablesort_sql($header);
  if (!empty($filter['where'])) {
    $result = pager_query($sql ." WHERE ". $filter['where'] . $tablesort, 50, 0, NULL, $filter['args']);
  }
  else {
    $result = pager_query($sql . $tablesort, 50);
  }

  while ($sniffer = db_fetch_object($result))
  {
    $post = unserialize($sniffer->post);
    if(count($post))
    {
    	$post = l(count($post), 'admin/reports/sniffer/event/'. $sniffer->wid);
    }
    else
    {
    	$post = '' ;
    }

    $rows[] = array('data' =>
      array( // Cells
				format_date($sniffer->timestamp, 'small'),
				theme('username', $sniffer), $sniffer->hostname,
				l(truncate_utf8($sniffer->location , 70, TRUE, TRUE), 'admin/reports/sniffer/event/'. $sniffer->wid, array('html' => TRUE)),
				$post
      ),
    );
  }

  if (!$rows) {
    $rows[] = array(array('data' => t('No log messages available.'), 'colspan' => 3));
  }

  $output .= theme('table', $header, $rows, array('id' => 'admin-sniffer'));
  $output .= theme('pager', NULL, 50, 0);

  return $output;
}

/**
 * Menu callback; displays details about a log message.
 */
function sniffer_event($id) {
  $output = '';
  $result = db_query('SELECT w.*, u.name, u.uid FROM {sniffer_data} w INNER JOIN {users} u ON w.uid = u.uid WHERE w.wid = %d', $id);
  if ($sniffer = db_fetch_object($result)) {

	if(variable_get("sniffer_use_browscap", 0))
	{  	$snifferbrowser = unserialize($sniffer->browser);
		$browser = array(    // to display main attr only
			'browser_name_pattern' => $snifferbrowser->browser_name_pattern,
			'parent' => $snifferbrowser->parent,
			'platform' => $snifferbrowser->platform,
			'win32' => $snifferbrowser->win32,
			'cookies' => $snifferbrowser->cookies,
			'javascript' => $snifferbrowser->javascript );
	}
	else
	{ $browser  =  unserialize($sniffer->browser) ;
	}

	$rows = array(
     array(
       array('data' => t('Date'), 'header' => TRUE),
       format_date($sniffer->timestamp, 'large'),
     ),
     array(
       array('data' => t('User'), 'header' => TRUE),
       theme('username', $sniffer),
     ),
     array(
       array('data' => t('User obj'), 'header' => TRUE),
       '<pre>'. print_r(unserialize($sniffer->user),1).'</pre>',
     ),
     array(
       array('data' => t('Location'), 'header' => TRUE),
       l($sniffer->location, $sniffer->location),
     ),
     array(
       array('data' => t('Referrer'), 'header' => TRUE),
       l($sniffer->referer, $sniffer->referer),
     ),
     array(
       array('data' => t('Hostname'), 'header' => TRUE),
       check_plain($sniffer->hostname),
     ),
     array(
       array('data' => t('POST'), 'header' => TRUE),
      '<pre>'. print_r(unserialize($sniffer->post),1).'</pre>',
     ),
     array(
       array('data' => t('Cookies'), 'header' => TRUE),
      '<pre>'. print_r(unserialize($sniffer->cookies),1).'</pre>',
     ),
    array(
       array('data' => t('Browser'), 'header' => TRUE),
      '<pre>'. print_r($browser,1).'</pre>',
     ),
   );
    $attributes = array('class' => 'sniffer-event');
    $output = theme('table', array(), $rows, $attributes);
  }
  return $output;
}

/**
 * Build query for sniffer administration filters based on session.
 */
function sniffer_build_filter_query() {
  if (empty($_SESSION['sniffer_overview_filter'])) {
    return;
  }

  $filters = sniffer_filters();

  // Build query
  $where = $args = array();
  foreach ($_SESSION['sniffer_overview_filter'] as $key => $filter) {
		$filter_where = array();
		foreach ($filter as $value) {
			$filter_where[] = $filters[$key]['where'];
			$args[] = $value;
		}
		if (!empty($filter_where)) {
			$where[] = '('. implode(' OR ', $filter_where) .')';
		}
  }
  $where = !empty($where) ? implode(' AND ', $where) : '';

  return array(
    'where' => $where,
    'args' => $args,
  );
}


function _sniffer_get_logged_uids() {
  $types = array();

	$result = db_query('SELECT DISTINCT(w.uid), u.name FROM {sniffer_data} w INNER JOIN {users} u ON w.uid = u.uid ORDER BY uid');
	while ($object = db_fetch_object($result)) {
    $types[$object->uid] = $object->name;
  }

  return $types;
}

/**
 * List sniffer administration filters that can be applied.
 */
function sniffer_filters() {
  $filters = array();

  foreach (_sniffer_get_logged_uids() as $uid => $name) {
    $types[$uid] = $name;
  }

  if (!empty($types)) {
    $filters['uid'] = array(
      'title' => t('UID'),
      'where' => "w.uid = %d",
      'options' => $types,
    );
  }

  return $filters;
}


/**
 * Return form for sniffer administration filters.
 *
 * @ingroup forms
 * @see sniffer_filter_form_submit()
 * @see sniffer_filter_form_validate()
 */
function sniffer_filter_form() {
  $session = &$_SESSION['sniffer_overview_filter'];
  $session = is_array($session) ? $session : array();
  $filters = sniffer_filters();

  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter log messages'),
    '#theme' => 'sniffer_filters',
    '#collapsible' => TRUE,
    '#collapsed' => empty($session),
  );
  foreach ($filters as $key => $filter) {
    $form['filters']['status'][$key] = array(
      '#title' => $filter['title'],
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 8,
      '#options' => $filter['options'],
    );
    if (!empty($session[$key])) {
      $form['filters']['status'][$key]['#default_value'] = $session[$key];
    }
  }

  $form['filters']['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );
  if (!empty($session)) {
    $form['filters']['buttons']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset')
    );
  }

  return $form;
}

/**
 * Validate result from sniffer administration filter form.
 */
function sniffer_filter_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Filter') && empty($form_state['values']['uid']) ) {
    form_set_error('type', t('You must select something to filter by.'));
  }
}

/**
 * Process result from sniffer administration filter form.
 */
function sniffer_filter_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = sniffer_filters();
  switch ($op) {
    case t('Filter'):
      foreach ($filters as $name => $filter) {
        if (isset($form_state['values'][$name])) {
          $_SESSION['sniffer_overview_filter'][$name] = $form_state['values'][$name];
        }
      }
      break;
    case t('Reset'):
      $_SESSION['sniffer_overview_filter'] = array();
      break;
  }
  return 'admin/reports/sniffer';
}
